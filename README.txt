Présentation

Ce module est l’approfondissement des modules M2103 (Bases de la programmation orientée objet) et M2104 (Bases de la conception orientée objet) abordés au S2. Il s’agit d’être capable de concevoir et d’implémenter des applications logicielles. Plus précisément, deux axes sont développés.

Le premier consiste en la production de conceptions logicielles détaillées à l’aide de patrons de conception (plus connus sous le nom de design patterns). Des éléments d’architecture logicielle seront assimilés, et la représentation UML sera utilisée.

Le deuxième consiste en l’utilisation des bonnes pratiques de la programmation orientée objet pour traduire des conceptions logicielles sous forme de code. L’utilisation d’un environnement de développement, la réalisation de tests unitaires sont autant d’outils permettant d’assurer la réutilisabilité et la qualité des composants logiciels créés.

Objectifs

Les objectifs de ce module sont :

Appliquer les principes SOLID durant la conception d’une application ;

Utiliser des patrons de conception (dans la représentation UML) adaptés aux problèmes de conception rencontrés ;

Traduire la représentation UML en Java, en utilisant les bonnes pratiques de la programmation orientée objet.

Documents

Les documents de cours (CM, énoncés de TD/TP et corrections) sont mis en ligne ci-dessous.

Contacts

Groupe Donatello : Dominique Bouthinon
Groupe Leonardo : Bouchaïb Lemaire
Groupe Michelangelo : Guillaume Santini
Groupe Raphael : Haïfa Zargayouna

Contacter prenom.nom@univ-paris13.fr pour toute question en dehors des séances

Organisation

Le module couvre 40 heures de formation découpées en 9 séances de 4 heures et 2 évaluations de 2h.

Calendrier prévisionnel Attention le corrigé est donné à titre indicatif, la modélisation notamment comporte des coquilles.